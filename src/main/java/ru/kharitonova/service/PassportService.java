package ru.kharitonova.service;

import io.smallrye.mutiny.Uni;
import ru.kharitonova.entity.PassportEntity;
import ru.kharitonova.entity.UserEntity;
import ru.kharitonova.repository.PassportRepository;
import ru.kharitonova.repository.UserRepository;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("/passport")
public class PassportService {

    @Inject
    PassportRepository repository;

    @GET
    @Path("/{id}")
    public Uni<PassportEntity> getPassportById(Long id) {
        return repository.findByPasswordId(id);
    }

    @GET
    @Path("user/{id}")
    public Uni<PassportEntity> getPassportByUserId(Long id) {
        return repository.findByUserId(id);
    }

    @GET
    @Path("/remove/{id}")
    public Uni<Void> removePasswordById(Long id) {
        return repository.removeByPasswordId(id);
    }

    @GET
    @Path("/removeByUser/{id}")
    public Uni<Void> removePasswordByUserId(Long id) {
        return repository.removeByUserId(id);
    }

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Uni<PassportEntity> save(PassportEntity entity) {
        return repository.save(entity);
    }

}
