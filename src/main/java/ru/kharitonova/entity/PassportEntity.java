package ru.kharitonova.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "t1_passport")
public class PassportEntity {

    @Id
    long id;

    @Column
    private LocalDateTime created;

    @Column
    private long user_id;

    @Column(length = 10)
    private String password_num;

    @Column(length = 1)
    private String valid;

    @Override
    public String toString() {
        return "PassportEntity{" +
                "id=" + id +
                ", created=" + created +
                ", user_id='" + user_id + '\'' +
                ", password_num='" + password_num + '\'' +
                ", valid='" + valid + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getPassword_num() {
        return password_num;
    }

    public void setPassword_num(String password_num) {
        this.password_num = password_num;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
}
